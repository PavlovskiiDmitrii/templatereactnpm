import React from 'react';

import PropTypes from 'prop-types';

import './MyCoolButton.scss';

const MyCoolButton = ({ title}) => (
  <button  className="container" >
    {title}
  </button>
);

MyCoolButton.propTypes = {
  title: PropTypes.string.isRequired
};

MyCoolButton.defaultProps = {
  onClick: () => {},
};

export default MyCoolButton;