import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, configure  } from 'enzyme';
import MyCoolButton from './MyCoolButton';

configure({adapter: new Adapter()});


describe('<MyCoolButton />', () => {

  it('should render component ', () => {
    const component = shallow(<MyCoolButton text={'test'}/>);
    const wrapper = component.find(".container");
    expect(wrapper.length).toBe(1);
  });  

  //Snapshot
  it('should render component Snapshot', () => {
    const component = render(<MyCoolButton text={'test'}/>);
    expect(component).toMatchSnapshot();
  });
});